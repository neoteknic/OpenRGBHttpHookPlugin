#ifndef HOOKSETTINGS_H
#define HOOKSETTINGS_H

#include "json.hpp"

using json = nlohmann::json;

class HookSettings
{
public:
    static void Save(json);
    static json Load();

private:
    static bool CreateSettingsDirectory();

    static const std::string SettingsFolder;
    static const std::string SettingsFileName;
};

#endif // HOOKSETTINGS_H
