#include "HookSettings.h"
#include <fstream>
#include <iostream>
#include <QFile>
#include <QString>
#include <QDir>
#include "OpenRGBHttpHookPlugin.h"

const std::string HookSettings::SettingsFolder = "/plugins/settings/";
const std::string HookSettings::SettingsFileName = "HttpHookSettings.json";

void HookSettings::Save(json Settings)
{
    if(!CreateSettingsDirectory())
    {
        printf("[OpenRGBHttpHookPlugin] Cannot create settings directory.\n");
        return;
    }

    std::ofstream File((OpenRGBHttpHookPlugin::RMPointer->GetConfigurationDirectory() + SettingsFolder + SettingsFileName), std::ios::out | std::ios::binary);

    if(File)
    {
        try{
            File << Settings.dump(4);
        }
        catch(const std::exception& e)
        {
            printf("[OpenRGBHttpHookPlugin] Cannot write settings: %s\n", e.what());
        }
        File.close();
    }
}

json HookSettings::Load()
{
    json Settings;

    std::ifstream SFile(OpenRGBHttpHookPlugin::RMPointer->GetConfigurationDirectory() + SettingsFolder + SettingsFileName, std::ios::in | std::ios::binary);

    if(SFile)
    {
        try
        {
            SFile >> Settings;
            SFile.close();
        }
        catch(const std::exception& e)
        {
            printf("[OpenRGBHttpHookPlugin] Cannot read settings: %s\n", e.what());
        }
    }

    return Settings;
}

bool HookSettings::CreateSettingsDirectory()
{
    std::string directory = OpenRGBHttpHookPlugin::RMPointer->GetConfigurationDirectory() + SettingsFolder;

    QDir dir(QString::fromStdString(directory));

    if(dir.exists())
    {
        return true;
    }

    return QDir().mkpath(dir.path());
}
