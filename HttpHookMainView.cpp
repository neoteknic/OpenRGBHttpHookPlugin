#include "HttpHookMainView.h"
#include "ui_HttpHookMainView.h"
#include "HookSettings.h"
#include "PluginInfo.h"
#include "TabHeader.h"
#include <QTimer>
#include <QTabBar>
#include <QToolButton>
#include <QAction>
#include <QMenu>
#include <QDialog>

HttpHookMainView::HttpHookMainView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::HttpHookMainView)
{
    ui->setupUi(this);

    // remove intial dummy tabs
    ui->tabs->clear();

    // define tab style + settings
    ui->tabs->setTabsClosable(true);
    ui->tabs->setStyleSheet("QTabBar::close-button{image:url(:images/close.png);}");
    ui->tabs->tabBar()->setStyleSheet("QTabBar::tab:hover {text-decoration: underline;}");

    QMenu* main_menu = new QMenu(this);

    QLabel* no_hook = new QLabel("You have no hook.\n You can add one by clicking the HTTPHook button.");
    no_hook->setAlignment(Qt::AlignCenter);

    // First tab: add button
    QPushButton* main_menu_button = new QPushButton();
    main_menu_button->setText("HTTPHook");
    ui->tabs->addTab(no_hook, QString(""));
    ui->tabs->tabBar()->setTabButton(0, QTabBar::RightSide, main_menu_button);
    ui->tabs->setTabEnabled(0, false);
    main_menu_button->setMenu(main_menu);

    QAction* new_measure = new QAction("New hook", this);
    connect(new_measure, &QAction::triggered, this, &HttpHookMainView::AddTabSlot);
    main_menu->addAction(new_measure);

    QAction* about = new QAction("About", this);
    connect(about, &QAction::triggered, this, &HttpHookMainView::AboutSlot);
    main_menu->addAction(about);

    auto hook_callback = [&](std::string verb, std::string path){

        bool ok = false;

        for(HttpHook* hook: hooks)
        {
            if(hook->verb == verb && hook->path == path)
            {
                hook->Run();
                ok = true;
            }
        }

        return ok;
    };

    server = new HttpServer();
    server->SetHookCallback(hook_callback);
    server->SetupRoute();

    QTimer::singleShot(6000, this, [=](){
        AutoLoad();
        UpdateStatus();
    });
}

HttpHookMainView::~HttpHookMainView()
{
    delete ui;
}

void HttpHookMainView::Start()
{
    server->Start(ui->ip->text().toStdString(), ui->port->value());
    UpdateStatus();

    running = true;

    ui->start_stop->setText("Stop");
}

void HttpHookMainView::Stop()
{
    server->Stop();
    UpdateStatus();

    running = false;

    ui->start_stop->setText("Start");
}

void HttpHookMainView::on_start_stop_clicked()
{
    if(running)
    {
        Stop();
    }
    else
    {
        Start();
    }
}

void HttpHookMainView::on_save_clicked()
{
    json j;

    std::vector<json> hooks_json;

    for(HttpHook* hook: hooks)
    {
        hooks_json.push_back(hook->ToJson());
    }

    j["hooks"] = hooks_json;

    j["ip"] = ui->ip->text().toStdString();
    j["port"] = ui->port->value();
    j["auto_start"] = ui->auto_start->isChecked();

    HookSettings::Save(j);
}

void HttpHookMainView::UpdateStatus()
{
    bool state = server->State();

    ui->status->setText(state ? "Running" : "Stopped");
}

void HttpHookMainView::AddTabSlot()
{
    AddTab();
}

void HttpHookMainView::AboutSlot()
{
    QDialog* dialog = new QDialog();
    dialog->setWindowTitle("HTTPHook");
    dialog->setMinimumSize(300,320);
    dialog->setModal(true);

    QVBoxLayout* dialog_layout = new QVBoxLayout(dialog);

    PluginInfo* plugin_info = new PluginInfo(dialog);

    dialog_layout->addWidget(plugin_info);

    dialog->exec();
}

HttpHook* HttpHookMainView::AddTab()
{
    HttpHook* hook = new HttpHook(this);

    int tab_size = ui->tabs->count();

    // insert at the end
    int tab_position = tab_size;

    std::string tab_name = hook->GetName();

    TabHeader* tab_header = new TabHeader();
    tab_header->Rename(QString::fromUtf8(tab_name.c_str()));

    hook->Rename(tab_name);

    ui->tabs->insertTab(tab_position, hook , "");
    ui->tabs->tabBar()->setTabButton(tab_position, QTabBar::RightSide, tab_header);

    ui->tabs->setCurrentIndex(tab_position);

    connect(hook, &HttpHook::Renamed, [=](std::string new_name){
        tab_header->Rename(QString::fromUtf8(new_name.c_str()));
    });

    connect(tab_header, &TabHeader::RenameRequest, [=](QString new_name){
        hook->Rename(new_name.toStdString());
    });

    connect(tab_header, &TabHeader::CloseRequest, [=](){
        int tab_idx = ui->tabs->indexOf(hook);

        ui->tabs->removeTab(tab_idx);

        hooks.erase(std::find(hooks.begin(), hooks.end(), hook));

        delete hook;
        delete tab_header;
    });

    ui->tabs->update();

    hooks.push_back(hook);

    return hook;
}

void HttpHookMainView::AutoLoad()
{
    json j = HookSettings::Load();

    if(j.contains("hooks"))
    {
        for(json hook_data : j["hooks"])
        {
            HttpHook* hook = AddTab();
            hook->FromJSON(hook_data);
        }
    }

    if(j.contains("ip"))
    {
        ui->ip->setText(QString::fromStdString(j["ip"]));
    }

    if(j.contains("port"))
    {
        ui->port->setValue(j["port"]);
    }

    if(j.contains("auto_start"))
    {
        bool auto_start = j["auto_start"];
        ui->auto_start->setChecked(auto_start);

        if(auto_start)
        {
            Start();
        }
    }
}
