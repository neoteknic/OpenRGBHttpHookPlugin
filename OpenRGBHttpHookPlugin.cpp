#include "OpenRGBHttpHookPlugin.h"
#include "HttpHookMainView.h"

bool OpenRGBHttpHookPlugin::DarkTheme = false;
ResourceManager* OpenRGBHttpHookPlugin::RMPointer = nullptr;

OpenRGBPluginInfo OpenRGBHttpHookPlugin::GetPluginInfo()
{
    printf("[OpenRGBHttpHookPlugin] Loading plugin info.\n");

    OpenRGBPluginInfo info;

    info.Name           = "Http hook plugin";
    info.Description    = "Expose webhooks to control lighting";
    info.Version        = VERSION_STRING;
    info.Commit         = GIT_COMMIT_ID;
    info.URL            = "https://gitlab.com/OpenRGBDevelopers/OpenRGBHttpHookPlugin";

    info.Icon.load(":/OpenRGBHttpHookPlugin.png");

    info.Location       =  OPENRGB_PLUGIN_LOCATION_TOP;
    info.Label          =  "HTTP Hook";
    info.TabIconString  =  "HTTP Hook";

    info.TabIcon.load(":/OpenRGBHttpHookPlugin.png");

    return info;
}

unsigned int OpenRGBHttpHookPlugin::GetPluginAPIVersion()
{
    printf("[OpenRGBHttpHookPlugin] Loading plugin API version.\n");

    return OPENRGB_PLUGIN_API_VERSION;
}

void OpenRGBHttpHookPlugin::Load(bool dark_theme, ResourceManager* resource_manager_ptr)
{
    printf("[OpenRGBHttpHookPlugin] Loading plugin.\n");

    RMPointer                = resource_manager_ptr;
    DarkTheme                = dark_theme;
}


QWidget* OpenRGBHttpHookPlugin::GetWidget()
{
    printf("[OpenRGBHttpHookPlugin] Creating widget.\n");

    return new HttpHookMainView();
}

QMenu* OpenRGBHttpHookPlugin::GetTrayMenu()
{
    return nullptr;
}

void OpenRGBHttpHookPlugin::Unload()
{
    printf("[OpenRGBHttpHookPlugin] Time to call some cleaning stuff.\n");
}

OpenRGBHttpHookPlugin::~OpenRGBHttpHookPlugin()
{
     printf("[OpenRGBHttpHookPlugin] Time to free some memory.\n");
}

