#include "HttpHook.h"
#include "ui_HttpHook.h"
#include "OpenRGBHttpHookPlugin.h"

#include <QMenu>
#include <QAction>
#include <QMainWindow>

HttpHook::HttpHook(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::HttpHook)
{
    ui->setupUi(this);

    GenerateID();

    ui->verb->addItems({"POST", "GET"});
    ui->action->addItems({"Load profile", "Turn OFF", "Effects plugin action"});

    ReloadSubActions();
}

HttpHook::~HttpHook()
{
    delete ui;
}

void HttpHook::Rename(std::string value)
{
    name = value;
}

std::string HttpHook::GetName()
{
    return name;
}

void HttpHook::Run()
{
    printf("Running Hook %s \n", path.c_str());

    int action_index = ui->action->currentIndex();

    switch (action_index) {
    case 0:
    {
        std::string profile = ui->sub_action->currentText().toStdString();
        printf("Scheduled task run: load profile [%s]\n", profile.c_str());

        OpenRGBHttpHookPlugin::RMPointer->GetProfileManager()->LoadProfile(profile);

        for(RGBController* controller : OpenRGBHttpHookPlugin::RMPointer->GetRGBControllers())
        {
            controller->UpdateLEDs();
        }

        break;
    }
    case 1:
    {
        for(RGBController* controller : OpenRGBHttpHookPlugin::RMPointer->GetRGBControllers())
        {
            controller->SetAllLEDs(ToRGBColor(0,0,0));
            controller->UpdateLEDs();
        }
        break;
    }
    case 2:
    {
        QString object_name = ui->sub_action->currentData().toString();

        for (QWidget *w : QApplication::topLevelWidgets())
        {
            if (QMainWindow* mainWin = qobject_cast<QMainWindow*>(w))
            {
                QAction* action = mainWin->findChild<QAction*>(object_name);

                if(action)
                {
                    action->trigger();
                }
            }
        }

    }
    default: break;
    }
}

void HttpHook::on_path_textChanged(const QString)
{
    path = ui->path->text().toStdString();

    if(path.empty())
    {
        GenerateID();
    }
}

void HttpHook::GenerateID()
{
    ui->path->setText(QString::fromStdString(std::to_string(rand())));
}

void HttpHook::ReloadSubActions()
{
    ui->sub_action->clear();

    int action_index = ui->action->currentIndex();

    switch (action_index) {
    case 0:
    {
        std::vector<std::string> profiles = OpenRGBHttpHookPlugin::RMPointer->GetProfileManager()->profile_list;

        for(std::string& profile: profiles)
        {
            ui->sub_action->addItem(QString::fromStdString(profile));
        }

        break;
    }
    case 2:
    {
        QRegularExpression action_filter("^OpenRGBEffectsPlugin::Action::");

        for (QWidget *w : QApplication::topLevelWidgets())
        {
            if (QMainWindow* mainWin = qobject_cast<QMainWindow*>(w))
            {
                QList<QAction*> profilesActions = mainWin->findChildren<QAction*>(action_filter);

                for(const QAction* action : profilesActions)
                {
                    ui->sub_action->addItem(action->property("OpenRGBEffectsPlugin::ActionTitle").toString(), action->objectName());
                }
            }
        }
        break;
    }
    default: break;
    }
}

void HttpHook::on_verb_currentIndexChanged(int)
{
    verb = ui->verb->currentText().toStdString();
}

void HttpHook::on_action_currentIndexChanged(int value)
{
    ui->sub_action->setVisible(value == 0 || value == 2);
    ReloadSubActions();
}

json HttpHook::ToJson()
{
    json j;

    j["verb"] =  ui->verb->currentIndex();
    j["path"] =  ui->path->text().toStdString();
    j["action"] = ui->action->currentIndex();
    j["name"] = name;

    switch (ui->action->currentIndex()) {
    case 0:
    case 2:
    {
        j["sub_action"] = ui->sub_action->currentText().toStdString();

        break;
    }
    default: break;
    }

    return j;
}

void HttpHook::FromJSON(json j)
{
    if(j.contains("verb"))
    {
        ui->verb->setCurrentIndex(j["verb"]);
    }

    if(j.contains("path"))
    {
        ui->path->setText(QString::fromStdString(j["path"]));
    }

    if(j.contains("action"))
    {
        ui->action->setCurrentIndex(j["action"]);
    }

    if(j.contains("sub_action"))
    {
        ui->sub_action->setCurrentText(QString::fromStdString(j["sub_action"]));
    }

    if(j.contains("name"))
    {
        name = j["name"];
        emit Renamed(name);
    }
}
